from django.conf.urls import patterns, include, url
from django.contrib import admin

"""SurveyConnector is central application
   all other application routing patterns should be included here
"""
urlpatterns = patterns('',
     (r'^', include('survey.urls')),
     (r'^admin/', include(admin.site.urls)),
)

"""add an additional routing pattern for the SurveyConnector app,
   which acts as a listener for signals from Facebook"""
urlpatterns += patterns('SurveyConnector.views',
    url('^listener', 'listener', name="survey_landing"),
)
