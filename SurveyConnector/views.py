#python imports
import random, time, os
from datetime import datetime
from collections import defaultdict

from contextlib import closing

#third party
from django.views.decorators.cache import never_cache
from django.shortcuts import render, render_to_response
from django.conf import settings
import facebook

#local imports
from fb.auth import get_token_from_code
from fb.fql import queries, get_profile_data
from fb.models import FacebookRespondent, FacebookProfile
from survey.models import Respondent


def base(request):
    return render(request, 'wave2/base.html', {'CLIENT_ID': settings.CLIENT_ID, 'BASE_URL': settings.BASE_URL})


def logout(request):
    return render(request, 'wave2/logout.html')


def close(request):
    return render(request, 'wave2/close.html')


@never_cache
def listener(request):
    """View to listen for notifications from Facebook that a user is trying to authenticate with our application.
    After receiving signal from Facebook, exchange the access code for a user access token and extract the su_id
    from the state variable
    """
    code = request.GET['code']
    token = get_token_from_code(code)
    #typicaly the state variable is used as a security measure,
    # but we are using it here to avoid complexities of data synchronization issues
    suid = request.GET['state']

    return extract_facebook_data(request, token, suid)


def extract_facebook_data(request, token, suid):
    """Extract the facebook friend data"""
    #initialize containers and objects
    fb_profiles = defaultdict(dict)
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #connect to Facebook using the user access token
    fb = facebook.GraphAPI(token)

    #get the number of respondent friends
    friend_cnt = len(fb.fql(queries['friend_cnt']))

    #open graph method of retrieving user info
    #r_profile = fb.get_object("me")

    #FQL approach
    r_profile = fb.fql(queries['userinfo'])[0]

    #extract the desired profile data and store in fb_profiles
    r_profile_data = get_profile_data(r_profile, timestamp)

    #get respondent metadata to associate with FacebookRespondentMetadata
    r_m = Respondent.objects.filter(su_id=int(suid))

    fp = FacebookProfile(**r_profile_data)
    fp.save()

    #save the FacebookRespondentMetadata
    facebook_respondent = FacebookRespondent(su_id=r_m[0],
                                profile=fp,
                                timestamp=timestamp,
                                friend_count=int(friend_cnt))
    facebook_respondent.save()

    return render_to_response('results.html', {'friends': {}})


