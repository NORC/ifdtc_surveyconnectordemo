from django.db import models

__all__ = ['Respondent', 'Response']

class Respondent(models.Model):
    su_id = models.IntegerField(primary_key=True)

class Response(models.Model):
    su_id = models.ForeignKey(Respondent)
    value = models.CharField(max_length=250)
    timestamp = models.DateTimeField()