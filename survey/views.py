from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext

from models import Respondent


def landing(request):
    return render_to_response('survey/screen.html', {}, context_instance=RequestContext(request))


def save_screen(request):
    r = Respondent(su_id=int(request.POST['r_suid']))
    r.save()

    return render_to_response('survey/q1.html', {'suid': request.POST['r_suid'],
                                                 'url': settings.BASE_URL,
                                                 'client': settings.CLIENT_ID},
                               context_instance=RequestContext(request))


def q1(request):
    return render_to_response('survey/resume.html', {})

def q2(request):
    return render_to_response('survey/resume.html', {})

def q3(request):
    return render_to_response('survey/resume.html', {})
