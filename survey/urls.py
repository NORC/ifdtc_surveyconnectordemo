from django.conf.urls import patterns, url

urlpatterns = patterns('survey.views',
    url('^$', 'landing', name="survey_landing"),
    url('^survey/save_screener', 'save_screen', name="save_screener"),
    url('^survey/q1$', 'q1', name="survey_q1"),
    url('^survey/q2$', 'q2', name="survey_q2"),
)
