"""
Django settings for IFDTC_SurveyConnector project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
import os

#DATABASE SETTINGS
DATABASES = {
    'default': {
        'NAME': 'survey_connect',
        'ENGINE': 'django.db.backends.sqlite3',
    }
}

#FACEBOOK SETTINGS
CLIENT_ID = '303856359773394'
CLIENT_SECRET = '03b16e927d59dab36d520eaa1cfcd633'

#PROJECT SETTINGS
DEBUG = True
TEMPLATE_DEBUG = True
SECRET_KEY = '&sdsdsdsd=0_g#b5xrlx(w!1#u+)sadlfjadslf;kgjal;dkfjsadlkfja;lsd-$5'
ROOT_URLCONF = 'SurveyConnector.urls'
SITE_ID = 1
BASE_URL = 'localhost:8000'

#Declare Apps
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    #internal apps
    'SurveyConnector',
    'fb',
    'survey',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    os.path.join(os.path.join(__file__, '..', 'templates')),
)