
__all__ = ['queries', 'get_profile_data']

"""Warning the FQL approach detailed below is recently deprecated,
   new applications should use Open Graph calls
"""

#create a list of the objects we want to collect from Facebook
user_fields = ['uid', 'third_party_id', 'first_name', 'middle_name', 'last_name', 'sex', 'locale', 'age_range', 'timezone']
_fields = ','.join(user_fields)

#create the FQL queries to extract data from facebook
queries = {"userinfo": """SELECT %s
                          FROM user
                          WHERE uid=me()""" % _fields,
           "friend_cnt": "SELECT uid2 FROM friend WHERE uid1=me()",
           "friendinfo": {"all": "SELECT uid2 FROM friend WHERE uid1=me()",
                          "friend_info": "SELECT %s FROM user WHERE uid IN (SELECT uid2 FROM #all)" % _fields}}


def get_profile_data(i):
    """Parse out the extracted Facebook data and store in a dictionary (key, value mapping)"""
    data = dict()
    data['third_party_id'] = i['third_party_id']
    data['first_name'] = i['first_name']
    data['middle_name'] = i['middle_name']
    data['last_name'] = i['last_name']
    data['sex'] = i['sex'][0] if i['sex'] else None
    data['country'] = i['locale']
    if 'age_range' in i:
        if 'min' in i['age_range']:
            data['age_min'] = i['age_range']['min']
        if 'max' in i['age_range']:
            data['age_max'] = i['age_range']['max']

    data['timezone'] = i['timezone']

    return data

