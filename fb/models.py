from django.db import models
from survey.models import Respondent

__all__ = ['FacebookRespondent', 'FacebookProfile']


#Database model from the FacebookProfile
class FacebookProfile(models.Model):
    third_party_id = models.CharField(max_length=250, primary_key=True)
    first_name = models.CharField(max_length=250, null=True)
    middle_name = models.CharField(max_length=250, null=True)
    last_name = models.CharField(max_length=250, null=True)
    sex = models.CharField(max_length=1, null=True)
    age_min = models.IntegerField(null=True)
    age_max = models.IntegerField(null=True)
    country = models.CharField(max_length=250, null=True)
    timezone = models.CharField(max_length=250, null=True)

#Create a FacebookRespondent model to store the respondents Facebook metadata and a link to the FacebookProfile
class FacebookRespondent(models.Model):
    su_id = models.ForeignKey(Respondent)
    profile = models.ForeignKey(FacebookProfile)
    timestamp = models.DateTimeField()
    friend_count = models.IntegerField()