import re
import requests
from django.conf import settings


def get_token_from_code(code):
    """Requests a user access token from Facebook by combining the client ID
       and application secret key
    """
    #create url for requesting access token from Facebook
    url = ''.join(['https://graph.facebook.com/oauth/access_token?',  #facebook auth url
                   'client_id=%s' % settings.CLIENT_ID,  #app id
                   '&redirect_uri=http://%s/listener' % settings.BASE_URL,  #app secret
                   '&client_secret=%s&code=%s' % (settings.CLIENT_SECRET, code)  #auth code from facebook
    ])

    #send a request to Facebook using above created url
    facebook_response = requests.get(url).text

    #use a regular expression to extract the access token
    return re.search('access_token=(.+?)&', facebook_response).groups()[0]
